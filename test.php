<? php

function sapLogin(){

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://h2h.unga.com:8081/unga-ecommerce-api-test/payment",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\r\n    \"CustomerName\":\"Jane Doe\",\r\n    \"TransID\":\"xxxxxx\",\r\n    \"TransDate\":\"2020-10-22 13:00:00\",\r\n    \"TransAmount\":\"1500\",\r\n    \"BusinessShortCode\":\"paybillNo.\",\r\n    \"MSISDN\":\"0700000000\",\r\n    \"OrderNo\":\"12345\"\r\n}",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiZWNvbW1lcmNlIiwiZXhwaXJhdGlvbiI6MTYwNDAzNjYwMiwiaXNzdWVkQXQiOjE2MDM5NTAyMDIsImJ5IjoiVU5HQSJ9.GyNgJ0tg7VhWJGGhTpDsfTHVSihSLJBrDSl0cAlJ0L0"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
}

function sendToSap(){
    
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://h2h.unga.com:8081/unga-ecommerce-api-test/payment",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\r\n    \"CustomerName\":\"Jane Doe\",\r\n    \"TransID\":\"xxxxxx\",\r\n    \"TransDate\":\"2020-10-22 13:00:00\",\r\n    \"TransAmount\":\"1500\",\r\n    \"BusinessShortCode\":\"paybillNo.\",\r\n    \"MSISDN\":\"0700000000\",\r\n    \"OrderNo\":\"12345\"\r\n}",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiZWNvbW1lcmNlIiwiZXhwaXJhdGlvbiI6MTYwNDAzNjYwMiwiaXNzdWVkQXQiOjE2MDM5NTAyMDIsImJ5IjoiVU5HQSJ9.GyNgJ0tg7VhWJGGhTpDsfTHVSihSLJBrDSl0cAlJ0L0"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
}



?>