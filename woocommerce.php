<?php
add_action( 'woocommerce_payment_complete', 'wdm_send_order_to_ext'); 
function wdm_send_order_to_ext( $order_id ){
// get order object and order details
$order = new WC_Order( $order_id ); 
$email = $order->billing_email;
$phone = $order->billing_phone;

// login 
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://h2h.unga.com:8081/unga-ecommerce-api-test/login",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\r\n    \"username\":\"ecommerce\",\r\n    \"key\":\"a8b7359afd7130e2edc8f5d3c4f9880562e41811\"\r\n}",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;

//Create the data object
$orderData = array(
    'customer_email' => $email,
    'customer_phone' => $phone
);

$apiData = array(
    'caller' => 'woocommerce',
    'json' => $orderData,
    'key' => 'MY_SECRET_KEY'
);

$jsonData =json_encode($orderData);

$url = "";
$api_mode = 'sandbox';
if($api_mode == 'sandbox'){
    // sandbox URL example
    $url = "https://h2h.unga.com:8081/unga-ecommerce-api-test/payment"; 
}
else{
    // production URL example
    $url = ""; 
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($jsonData));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$response = curl_exec ($ch);

curl_close ($ch);

// the handle response    
if (strpos($response,'ERROR') !== false) {
        print_r($response);
} else {
        // success
}
}